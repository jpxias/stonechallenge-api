import { InternalServerErrorException, Logger } from '@nestjs/common';
import { IBaseService } from './IBase.service';
import { Model } from 'dynamoose/dist/Model';
import { AnyItem } from 'dynamoose/dist/Item';
import { v4 as uuid } from 'uuid';

export class BaseService<T> implements IBaseService<T> {
  constructor(private readonly entity: Model<AnyItem>) {}
  private logger = new Logger('BaseService');

  create(item: AnyItem): Promise<T> {
    try {
      return new Promise<T>((resolve, reject) => {
        item.id = uuid();
        this.entity.create(item, (err, res: any) => {
          if (err) {
            reject(err);
          } else {
            resolve(res);
          }
        });
      });
    } catch (error) {
      this.logger.error(
        `[create] Error creating new item for entity: ${this.entity.name}. ${JSON.stringify(error)}`,
      );
      throw new InternalServerErrorException(error);
    }
  }

  findOne(params: Object): Promise<T> {
    return new Promise<any>((resolve, reject) => {
      try {
        this.entity
          .query(params)
          .limit(1)
          .exec((err, res) => {
            if (err) {
              reject(err);
            } else {
              if (res.length) {
                resolve(res[0]);
              } else {
                resolve(null);
              }
            }
          });
      } catch (error) {
        this.logger.error(
          `[FindOne] Error finding entity: ${this.entity.name}. ${JSON.stringify(error)}`,
        );
        reject(error);
      }
    });
  }

  findAll(): Promise<T[]> {
    try {
      return new Promise<any>((resolve, reject) => {
        this.entity.scan().exec((err, res) => {
          if (err) {
            this.logger.error(
              `[findById] Error finding entity: ${this.entity.name}. ${JSON.stringify(err)}`,
            );
            reject(err);
          } else {
            resolve(res);
          }
        });
      });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async findById(id: string): Promise<AnyItem> {
    try {
      return this.entity.get(id);
    } catch (error) {
      this.logger.error(
        `[findById] Error finding entity: ${this.entity.name}. ${JSON.stringify(error)}`,
      );
      throw new InternalServerErrorException(error);
    }
  }

  async delete(id: string) {
    try {
      const item = await this.findById(id);
      await this.entity.delete(id);
      return item;
    } catch (error) {
      this.logger.error(
        `[delete] Error deleting entity: ${this.entity.name}. ${JSON.stringify(error)}`,
      );
      throw new InternalServerErrorException(error);
    }
  }
}
