import { AnyItem } from 'dynamoose/dist/Item';

export interface IBaseService<T> {
  findAll(): Promise<T[]>;
  findById(id: string): Promise<AnyItem>;
  findOne(params: Object): Promise<T>;
  create(entity: AnyItem): Promise<T>;
  delete(id: string);
}
