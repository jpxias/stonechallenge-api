import { HttpStatus, Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getApplicationStatus(): HttpStatus {
    return HttpStatus.OK;
  }
}
