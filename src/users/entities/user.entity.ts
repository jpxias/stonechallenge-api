import * as bcrypt from 'bcrypt';
import * as dynamoose from 'dynamoose';

const UsersSchema = new dynamoose.Schema({
  id: {
    type: String,
    hashKey: true,
  },
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    index: {
      name: 'emailIndex',
    },
  },
  password: {
    type: String,
    set: (value) => bcrypt.hashSync(value, bcrypt.genSaltSync(8), null),
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
});

const User = dynamoose.model('Users', UsersSchema);

User.methods.item.set(
  'validatePassword',
  async function (password): Promise<boolean> {
    return bcrypt.compareSync(password, this.password);
  },
);

export type UserType = typeof User;

export default User;
