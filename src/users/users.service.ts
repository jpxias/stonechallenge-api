import { BadRequestException, Injectable } from '@nestjs/common';

import User from './entities/user.entity';
import { BaseService } from 'src/base/base.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { AnyItem } from 'dynamoose/dist/Item';

@Injectable()
export class UsersService extends BaseService<typeof User> {
  constructor() {
    super(User);
  }

  async findById(id: string): Promise<AnyItem> {
    const user = await super.findById(id);
    return user;
  }

  async createUser(createUserDTO: CreateUserDTO) {
    if (
      !createUserDTO.email ||
      !createUserDTO.name ||
      !createUserDTO.password
    ) {
      throw new BadRequestException('Preencha todos os campos obrigatórios.');
    }

    const existentUser = await this.findOne({ email: createUserDTO.email });

    if (existentUser) {
      throw new BadRequestException(
        'Este email já está cadastrado em nosso sistema.',
      );
    }
    const user = new User({
      name: createUserDTO.name,
      email: createUserDTO.email,
      password: createUserDTO.password,
      role: 'USER',
    });

    return super.create(user);
  }
}
