import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserType } from 'src/users/entities/user.entity';

export const GetUser = createParamDecorator(
  (data, ctx: ExecutionContext): UserType => {
    const req = ctx.switchToHttp().getRequest();
    return req.user;
  },
);
