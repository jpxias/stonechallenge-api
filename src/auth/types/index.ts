export interface JwtPayload {
  sub: string;
  iss: string;
  iat: number;
  id: string;
}

export interface TokenPayload {}
