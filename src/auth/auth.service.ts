import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOne({ email });

    if (!user) {
      throw new NotFoundException(
        'Este email não se encontra registrado no sistema.',
      );
    }

    const isMatch = await user.validatePassword(password);

    if (isMatch) {
      return user;
    }

    return null;
  }

  async getUser(user: any) {
    return this.usersService.findById(user.id);
  }

  async login(user: any) {
    const payload = { email: user.email, id: user.id };

    return {
      access_token: this.jwtService.sign(payload),
      sub: user.id,
    };
  }
}
