import {
  Controller,
  Post,
  UseGuards,
  Get,
  Logger,
  Request,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { GetUser } from './decorators/get-user.decorator';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { ApiBearerAuth, ApiBody, ApiProperty } from '@nestjs/swagger';
import { UserType } from 'src/users/entities/user.entity';

class Login {
  @ApiProperty()
  username: string;

  @ApiProperty()
  password: string;
}

@Controller('auth')
export class AuthController {
  private logger = new Logger('AuthController');

  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @ApiBody({ type: Login, required: true })
  @Post('login')
  async login(@Request() req) {
    this.logger.verbose(`Login for user: ${req.user.email}`);

    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT')
  @Get('session')
  async getSession(
    @GetUser()
    user: UserType,
  ) {
    this.logger.verbose(`Get Session for user: ${JSON.stringify(user)}`);
    delete user.password;
    return user;
  }
}
