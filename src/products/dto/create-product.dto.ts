import { ApiProperty } from '@nestjs/swagger';

export class CreateProductoDTO {
  @ApiProperty({ required: true })
  name: string;

  @ApiProperty({ required: true })
  serial: string;
}
