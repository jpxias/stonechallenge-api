import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import Product from './entities/products.entity';
import { BaseService } from 'src/base/base.service';
import { CreateProductoDTO } from './dto/create-product.dto';

@Injectable()
export class ProductsService extends BaseService<typeof Product> {
  constructor() {
    super(Product);
  }

  async createProduct(
    createProductDTO: CreateProductoDTO,
  ): Promise<typeof Product> {
    try {
      if (!createProductDTO.name || !createProductDTO.serial) {
        throw new BadRequestException('Preencha todos os campos obrigatórios.');
      }

      const testProduct = new Product(createProductDTO);

      return this.create(testProduct);
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }
}
