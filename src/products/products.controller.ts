import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductoDTO } from './dto/create-product.dto';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard, RolesGuard } from 'src/auth';
import { CheckRole } from 'src/auth/decorators/permissions.decorator';
import { ROLES } from 'src/auth/permissions.enum';

@Controller('products')
@ApiBearerAuth('JWT')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  async getProducts(): Promise<any> {
    return this.productsService.findAll();
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  async getProduct(@Param('id') id: string): Promise<any> {
    return this.productsService.findById(id);
  }

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @CheckRole(ROLES.MANAGER)
  async createProduct(
    @Body() createProductDTO: CreateProductoDTO,
  ): Promise<any> {
    return this.productsService.createProduct(createProductDTO);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @CheckRole(ROLES.MANAGER)
  async delete(@Param('id') id: string): Promise<any> {
    return this.productsService.delete(id);
  }
}
