import * as dynamoose from 'dynamoose';

const ProductsSchema = new dynamoose.Schema({
  id: {
    type: String,
    hashKey: true,
  },
  name: {
    type: String,
    required: true,
  },
  serial: {
    type: String,
    required: true,
  },
});

const Products = dynamoose.model('Products', ProductsSchema);

export default Products;
