import dynamoose from 'dynamoose';

const dynamoDB =
  process.env.NODE_ENV === 'development'
    ? new dynamoose.aws.ddb.local()
    : new dynamoose.aws.ddb.DynamoDB({
        credentials: {
          accessKeyId: process.env.AWS_KEY,
          secretAccessKey: process.env.AWS_SECRET,
        },
        region: process.env.AWS_REGION,
      });

export default dynamoDB;
