import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const buildSwagger = (app: INestApplication<any>) => {
  const config = new DocumentBuilder()
    .setTitle('Stone Challenge API')
    .setDescription('Documentação de API do desafio da Stone.')
    .setVersion('1.0')
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT',
        description: 'Adicione o token JWT',
        in: 'header',
      },
      'JWT',
    )
    .addServer('/dev')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
};
